FROM frolvlad/alpine-oraclejdk8:slim
VOLUME /tmp
ADD target/product-rest-w-hsqldb.war product-rest-w-hsqldb.war
ENV JAVA_OPTS=""
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /product-rest-w-hsqldb.war"]
