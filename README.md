# README #

In order to get this application running using "mvn clean install -DskipTests" and then "mvn spring-boot:run". 
Use http://localhost:8080/api/products to see the listing of all products


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### todo ###
* Add HATEOAS for resource linking
* Add unit tests
* Add integration tests
* Add performance tests
* Use CICD (Jenkins)
* Add static code analysis (jacoco)
* Add runtime code coverage (SonarQube)
* Deploy on Cloud (AWS)
* Test AWS autoscaling by adding some load on the application
* Add support for multiple DBs (PostgreSQL)
* Add support for multiple DBs (Cassandra)
* Add support for multiple DBs (MongoDB)
* Integrate with Swagger for API documentation
* Deploy on Cloud (PCF)
* Dockerize the app
* Integrate with Hystrix, a circuit breaker (Netflix)
* Integrate with Ribbon, client-side load balancer (Netflix)
* Integrate with Feign, a declarative REST client (Netflix)
* Integrate with Eureka, automatic service discovery (Netflix)
* Integrate with Angular4 framework
* Integrate with security (Basic Auth, OAuth, JWT) 
* Integrate with messaging (RabbitMQ, Kafka, SQS)