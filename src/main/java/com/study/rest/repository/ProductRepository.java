package com.study.rest.repository;

import com.study.rest.domain.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
//Use the following for pagination
//public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

}
