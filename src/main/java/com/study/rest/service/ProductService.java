package com.study.rest.service;

import com.study.rest.domain.Product;
import com.study.rest.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product getProduct(Long productId) {
        return productRepository.findOne(productId);
    }

    public void deleteProduct(Long productId) {
        productRepository.delete(productId);
    }

}
